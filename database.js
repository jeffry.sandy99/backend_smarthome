const mysql = require("mysql2");

// Raspi
// var connection = mysql.createConnection({
//     host: "localhost",
//     user: "admin",
//     password: "qwert12345",
//     database: "BackendSmartHome"
// })

// Mac
var connection = mysql.createConnection({
    host: "localhost",
    user: "admin",
    password: "qwert12345",
    database: "BackendSmartHome"
})

connection.connect(function (error){
    if(error){
        console.log(error);
    }else{
        console.log("Database Connection Success");
    }
})

module.exports = connection;