const convertHexToBinary = (hexData) => {
  var binaryArray = [];
  for (let i = 0; i < 7; i += 1) {
    var binaryBit = Number(hexData[i]).toString(2);
    while (binaryBit.length < 8) {
      binaryBit = "0" + binaryBit;
    }
    // console.log("Binary Bit Raw");
    // console.log(binaryBit);
    // console.log("Binary Bit Array");
    for (let j = 0; j < 8; j += 1) {
      let char = binaryBit[j];
      // console.log(char);
      binaryArray.push(char);
    }
  }
  console.log("========================");
  // });
  return binaryArray;
};

const convertBinaryToJSON = (data) => {
  var wemos_id = 0;
  var ldr_value = 0;
  var relay_status = 0;
  var dimmer_value = 0;
  var suhu = 0;
  var kelembapan = 0;
  var door_status = 0;
  var ipAddress_A = 0;
  var ipAddress_B = 0;

  wemos_id |= data[0] << 2;
  wemos_id |= data[1] << 1;
  wemos_id |= data[2];

  ldr_value |= data[3] << 9;
  ldr_value |= data[4] << 8;
  ldr_value |= data[5] << 7;
  ldr_value |= data[6] << 6;
  ldr_value |= data[7] << 5;
  ldr_value |= data[8] << 4;
  ldr_value |= data[9] << 3;
  ldr_value |= data[10] << 2;
  ldr_value |= data[11] << 1;
  ldr_value |= data[12];

  relay_status |= data[13];

  ipAddress_A |= data[14] << 7;
  ipAddress_A |= data[15] << 6;
  ipAddress_A |= data[16] << 5;
  ipAddress_A |= data[17] << 4;
  ipAddress_A |= data[18] << 3;
  ipAddress_A |= data[19] << 2;
  ipAddress_A |= data[20] << 1;
  ipAddress_A |= data[21];

  ipAddress_B |= data[22] << 7;
  ipAddress_B |= data[23] << 6;
  ipAddress_B |= data[24] << 5;
  ipAddress_B |= data[25] << 4;
  ipAddress_B |= data[26] << 3;
  ipAddress_B |= data[27] << 2;
  ipAddress_B |= data[28] << 1;
  ipAddress_B |= data[29];

  dimmer_value |= data[30] << 9;
  dimmer_value |= data[31] << 8;
  dimmer_value |= data[32] << 7;
  dimmer_value |= data[33] << 6;
  dimmer_value |= data[34] << 5;
  dimmer_value |= data[35] << 4;
  dimmer_value |= data[36] << 3;
  dimmer_value |= data[37] << 2;
  dimmer_value |= data[38] << 1;
  dimmer_value |= data[39];
  
  suhu |= data[40] << 5;
  suhu |= data[41] << 4;
  suhu |= data[42] << 3;
  suhu |= data[43] << 2;
  suhu |= data[44] << 1;
  suhu |= data[45];

  kelembapan |= data[46] << 6;
  kelembapan |= data[47] << 5;
  kelembapan |= data[48] << 4;
  kelembapan |= data[49] << 3;
  kelembapan |= data[50] << 2;
  kelembapan |= data[51] << 1;
  kelembapan |= data[52];

  door_status |= data[53] << 1;
  door_status |= data[54];

  return {
    wemos_id: wemos_id,
    ldr_value: ldr_value,
    relay_status: relay_status,
    dimmer_value: dimmer_value,
    suhu: suhu,
    kelembapan: kelembapan,
    door_status: door_status,
    ipAddress_A: ipAddress_A,
    ipAddress_B: ipAddress_B,
  };
};

const checkerWemosIdInputData = (jsonData) => {
    if (jsonData.wemos_id == 1){

    }
}

module.exports = {
    convertHexToBinary,
    convertBinaryToJSON,
    checkerWemosIdInputData
}