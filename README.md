# Backend_SmartHome

## Getting started

README

## All Wemos Target server to -> 192.168.0.112
Data Model Wemos ID
1: Lampu, Relay, LDR     -> Update wemosId, relay_status, ldr_value, ipAddress_A, ipAddress_B, other's null
-   Request ke Wemos -> On - Off  -> val 1 0
-   IPAddress: 192.168.0.101
2: Lampu, Relay, LDR     -> Update wemosId, relay_status, ldr_value, ipAddress_A, ipAddress_B, other's null
-   Request ke Wemos -> On - Off  -> val 1 0
-   IPAddress: 192.168.0.102
3: Lampu, Relay, LDR     -> Update wemosId, relay_status, ldr_value, ipAddress_A, ipAddress_B, other's null
-   Request ke Wemos -> On - Off  -> val 1 0
-   IPAddress: 192.168.0.103
4: Lampu, Relay, Dimmer  -> Update wemosId, relay_status, dimmer_value, ipAddress_A, ipAddress_B
-   Request ke Wemos -> PWM Val   -> value langsung PWM (int), relay (string on & off)
-   IPAddress: 192.168.0.104
-   ESP Board Version: 2.4.2, newer error for dimmer Robotdyn
5: IR AC                 -> Update wemosId, suhu, kelembapan, ipAddress_A, ipAddress_B
-   Request ke Wemos -> On - Off  -> val 1 0
-   IPAddress: 192.168.0.105
6: Solenoid Relay, Reed  -> Update wemosId, relay_status, doorstatus, ipAddress_A, ipAddress_B
-   Request ke Wemos -> On - Off  -> val 1 0
-   IPAddress: 192.168.0.106

### ALL type Wemos: -1 => Request Monitoring Data


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:8980102f8031bde42be3a54f17bbf521?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:8980102f8031bde42be3a54f17bbf521?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:8980102f8031bde42be3a54f17bbf521?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jeffry.sandy99/backend_smarthome.git
git branch -M main
git push -uf origin main
```