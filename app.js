// Node.js socket server script
const database = require('./database');
const net = require("net");
const {updateDatabase, ref, databaseFIR, onValue} = require("./firebaseSettings")
const {
  convertHexToBinary,
  convertBinaryToJSON,
} = require("./prosesData");

var clientConnected = [];
var counterUpdate = 0;
var wemosId_Address = {
  1: "192.168.50.101",
  2: "192.168.50.102",
  3: "192.168.50.103",
  4: "192.168.50.104",
  5: "192.168.50.105",
  6: "192.168.50.106",
};

// // Create a server object
const server = net.createServer((socket) => {
  socket.on("data", (data) => {

    // Convert Hex to Binary
    var binary = convertHexToBinary(data);

    // Convert Binary to JSON Data
    var json = convertBinaryToJSON(binary);

    // Wemos will only send data when triggered,
    // every incoming data will be updated
    updateDatabase(json);
  });
  // socket.write("SERVER: Hello! This is server speaking.<br>");
  // socket.end("SERVER: Closing connection now.<br>");
  socket.on("error", (err) => {
    socket.write("SERVER: Hello! This is server speaking.<br>");
    // socket.end("SERVER: Error.<br>");
    console.error(err);
  });
});

// Open server on port 8000
server.listen(8000, () => {
  console.log("opened server on", server.address().port);
});

server.on("connection", (client) => {
  console.log(typeof client);
  console.log(
    `New Connection from ${client.remoteAddress} and ${client.remotePort}`
  );

  // Add Connected Wemos Client to local Array
  clientConnected.push(client);
});

// Listening Firebase RDB Control
const reference = ref(databaseFIR, 'Control');
  onValue(reference, (snapshot) => {
    // If There's any changes, will send data to Wemos
    const data = snapshot.val();
    console.log("There's Request Control")
    console.log(data.wemos_id);
    console.log("Updating IP: " + wemosId_Address[data.wemos_id] + ", Val: " + data.value);

    // Send Data to Wemos with selected IP Address
    controlWemosId(wemosId_Address[data.wemos_id], data.value);
})

function controlWemosId(ip_address, value){
  // Set IP Address to which wemos to command
  // value options:
  // "-1" => Ask Monitoring Data
  // "on" => Switch relay on
  // "off" => Switch relay off

  // Filter for the same IP Address
  let selectedClient = clientConnected.filter((client) => {
    let ipAddr = client.remoteAddress.replace('::ffff:', '')
    return (ipAddr.localeCompare(ip_address) == 0)
  });

  // If there's then send command to wemos with Selected IP Address
  if (selectedClient.length > 0){ 
    console.log("Get Same Client IP");
    console.log(selectedClient[0].remoteAddress, " - ", value)
    selectedClient[0].write(`${value}`);
  }
  return selectedClient
}

// setInterval(() => {
//   // Update Firebase data every 5 seconds
//   // Selecting function TryData return
//   if (counterUpdate == 6){
//     counterUpdate = 0
//   }
//   counterUpdate = counterUpdate + 1;

  // TESTING Minta Data buat setiap client
  // clientConnected.forEach((client) => {
  //   client.write("1");
  // });

  // TESTING
  // console.log("Ask Relay Change");
  // console.log(tryData());
  // controlWemosId('192.168.0.101', tryData());
  // controlWemosId('192.168.0.102', tryData());
  // controlWemosId('192.168.0.103', tryData());
  // controlWemosId('192.168.0.104', tryData());
  // controlWemosId('192.168.0.105', tryData());

  // TESTING UPDATE
  // // WEMOS 1, 2, 3
  // updateDatabase({
  //   wemos_id: 3,
  //   ldr_value: 100,
  //   relay_status: 0,
  //   dimmer_value: null,
  //   suhu: null,
  //   kelembapan: null,
  //   door_status: null,
  //   ipAddress_A: 0,
  //   ipAddress_B: 103
  // })

  // // WEMOS 4 (Dimmer)
  // updateDatabase({
  //   wemos_id: 4,
  //   ldr_value: null,
  //   relay_status: 1,
  //   dimmer_value: null,
  //   suhu: null,
  //   kelembapan: null,
  //   door_status: null,
  //   ipAddress_A: 0,
  //   ipAddress_B: 104
  // })

  // // WEMOS 5
  // updateDatabase({
  //   wemos_id: 5,
  //   ldr_value: null,
  //   relay_status: null,
  //   dimmer_value: null,
  //   suhu: 25,
  //   kelembapan: 70,
  //   door_status: null,
  //   ipAddress_A: 0,
  //   ipAddress_B: 105
  // })
// }, 5000);

// Func for Dummy Data
function tryData(){
  if(counterUpdate == 1){
    return "-1"
  }else if(counterUpdate == 2){
    return "on"
  }else if(counterUpdate == 3){
    return "off" 
  }else if(counterUpdate == 4){
    return "30" 
  }else if(counterUpdate == 5){
    return "70" 
  }
}