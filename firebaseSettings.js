// Import the functions you need from the SDKs you need
const { initializeApp } = require("firebase/app");
const {getDatabase, set, ref, onValue} = require("firebase/database");
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA3CnnoeQN0jbB2RKeRXsZ9JFeQMtQu97M",
  authDomain: "smarthome-system-6bb2c.firebaseapp.com",
  databaseURL: "https://smarthome-system-6bb2c-default-rtdb.firebaseio.com",
  projectId: "smarthome-system-6bb2c",
  storageBucket: "smarthome-system-6bb2c.appspot.com",
  messagingSenderId: "711782763656",
  appId: "1:711782763656:web:d554ec639872e25f46633d",
  measurementId: "G-GPFGZEJQ0Y"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const databaseFIR = getDatabase(app);

function updateDatabase(data){
  const {
    wemos_id,
    ldr_value,
    relay_status,
    dimmer_value,
    suhu,
    kelembapan,
    door_status,
    ipAddress_A,
    ipAddress_B
  } = data;

  set(ref(databaseFIR, "W" + wemos_id), {
    wemos_id: wemos_id,
    ldr_value: ldr_value,
    relay_status: relay_status,
    dimmer_value: dimmer_value,
    suhu: suhu,
    kelembapan: kelembapan,
    door_status: door_status,
    ipAddress: ipAddress_A + "." + ipAddress_B
  });
}

module.exports = {
  updateDatabase,
  ref,
  onValue,
  databaseFIR
};